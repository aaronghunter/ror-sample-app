module ApplicationHelper

	# Returns the full title, per-page basis.				# Comment

	def full_title(page_title)								# Method Def
		base_title = "Ruby on Rails Tutorial Sample App"	# Var Assignment
		if page_title.empty?								# Boolean Test
			base_title										# Implicit Return
		else
			"#{base_title} | #{page_title}"					# String Interpolated Return
		end
	end
end
